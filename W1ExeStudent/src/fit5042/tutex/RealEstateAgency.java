package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	Property property1 = new Property(1, "316 Neerim Road, Carnegie VIC 3163, Australia", 2, 87, 489000);
    	Property property2 = new Property(2, "93 Atherton Road, Oakleigh VIC 3166, Australia", 3, 270, 965000);
    	Property property3 = new Property(3, "285 La Trobe Street, Melbourne VIC 3000, Australia", 2, 57, 570000);
    	Property property4 = new Property(4, "777 Glen Huntly Road, Caulfield VIC 3162, Australia", 3, 300, 900000);
    	Property property5 = new Property(5, "173 City Road, Southbank VIC 3006, Australia", 2, 76, 570000);
    	try {
			propertyRepository.addProperty(property1);
			propertyRepository.addProperty(property2);
			propertyRepository.addProperty(property3);
			propertyRepository.addProperty(property4);
			propertyRepository.addProperty(property5);
			System.out.println("5 properties added successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("add property unsuccessfully");
		}
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
		try {
			List<Property> allProperties = propertyRepository.getAllProperties();
			allProperties = propertyRepository.getAllProperties();
			for (Property p : allProperties) {
				System.out.println(p.toString());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("the property list is empty");
		}
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	int userInput = 0;
    	Scanner sc = new Scanner(System.in);
    	System.out.println("Enter the ID of the property you want to search:");
    	try {
    		userInput = sc.nextInt();
    		Property property = propertyRepository.searchPropertyById(userInput);
    		if (property != null) {
    			System.out.println(property.toString());
    		} else {
    			System.out.println("there is no such property in property list");
    		}
    	} catch (Exception e) {
    		System.out.println("Please enter a number ");
    	}
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
